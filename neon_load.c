#include <stdio.h>
#include <arm_neon.h>

unsigned short int A[] = {1,2,3,4};     // array with 4 elements

int main(void)
{
    uint16x4_t v;         // declare a vector of four 16-bit lanes
    v = vld1_u16(A);    // load the array from memory into a vector
    v = vadd_u16(v, v); // double each element in the vector
    vst1_u16(A, v);     // store the vector back to memory

    printf("[0] = %d\n", A[0]);

    return 0;
}
