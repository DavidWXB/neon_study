#include <stdio.h>

int main()
{
    int a = 0;
    int b = 1;
    printf("a = %d, b = %d\n", a, b);
    
    __asm__ __volatile__("mov %1, %0":"=r" (a) : "r" (b));

    printf("a = %d, b = %d\n", a, b);

    return 0;
}
