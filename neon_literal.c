#include <stdio.h>
#include <arm_neon.h>

int main(void)
{
    uint8x8_t v;        // define v as a vector with 8 lanes of 8-bit data
    unsigned char A[8]; // allocate memory for eight 8-bit data

    v = vcreate_u8(0x0102030405060708);     // create a vector that contains
                                            // the values 1,2,3,4,5,6,7,8
    vst1_u8(A, v);      // store the vector to memory, in this case, to array A

    printf("[7] = %d\n", A[7]);

    return 0;
}
