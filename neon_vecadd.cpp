#include <vector>
#include <iostream>
#include <arm_neon.h>
using namespace std;


void matmul()
{
    int32_t a[500], b[500], c[500];
    for (int i = 0; i < 500; i++) {
        a[i] = i; b[i] = 500 - i;
        cout<<a[i]<<' '<<b[i]<<endl;
    }

    for(int i = 0; i < 500; i += 4) {
        int32x4_t at = vld1q_s32(a+i);
        int32x4_t bt = vld1q_s32(b+i);
        int32x4_t ct = vaddq_s32(at, bt);
        vst1q_s32(c+i, ct);
    }

    int sum = 0;
    for(int i = 0; i < 500; i++) {
        sum += c[i];
    }
    cout<<"sum = "<<sum<<endl;
}


int main()
{
    matmul();

    return 0;
}
