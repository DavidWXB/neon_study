#include <stdio.h>
#include <arm_neon.h>

uint32_t vector_add_of_n(uint32_t* ptr, uint32_t items)
{
    uint32_t result, *i;
    uint32x2_t vec64a, vec64b;
    uint32x4_t vec128 = vdupq_n_u32(0);     // clear acccumulators

    for(i = ptr, i < (ptr + (items/4)); i += 4)
    {
        uint32x4_t temp128 = vld1q_u32(i);  // load four 32-bit values
        vec128 = vaddq_u32(vec128, temp128);// add 128-bit vectors
    }

    vec64a = vget_low_u32(vec128);      // split 128-bit vector
    vec64b = vget_high_u32(vec128);     // into two 64-bit vectors
    vec64a = vadd_u32(vec64a, vec64b);  // add 64-bit vectors together

    result = vget_lane_u32(vec64a, 0);  // extract lanes and
    result += vget_lane_u32(vec64a, 1); // add together scalars

    return result;
}
